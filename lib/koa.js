import Koa from 'koa'
import Router from 'koa-router'
import koaBody from 'koa-body'

const app = new Koa()
const router = new Router()

/* ********HANDLERS******** */

const uploadForm = async ( ctx ) => {
    ctx.body = `<form action="fileupload" method="post" enctype="multipart/form-data">
    <input type="file" name="filetoupload"><br>
    <input type="submit">
    </form>`
}

const handleUpload = async ( ctx ) => {
    console.log( 'RICUEST: ', JSON.stringify( ctx.request, null, 4 ) )
}

/* *********ROUTES********* */

router.get( '/', uploadForm )

router.post( '/fileupload', koaBody(), handleUpload )

/* ******GLOBAL MIDDLEWARES******* */

// Logger
app.use( async ( ctx, next ) => {
    const start = Date.now()
    await next()
    const ms = Date.now() - start
    console.log( `${ctx.method} ${ctx.url} - ${ms}ms - ${ctx.status}` )
} )

// Routes

app.use( router.routes() )

/* ******SERVER******* */

app.listen( 8800 )
